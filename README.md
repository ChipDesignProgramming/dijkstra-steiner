Summary
-------
This repository contains setup and example instances for an exercise of implementing the Dijkstra Steiner Algorithm for 2D rectilinear instances.

Compilation
-------
If `cmake` is available, you can compile the program using the following command in the root directory of the project.
```
cmake . && make dijkstra-steiner
```

Usage
-----
```
./dijkstra-steiner instance_file
```

Formatting
----------
If you do not want to worry about formatting at all, you might want to use an automatic formatting tool, for example `clang-format`.
A possible `.clang-format` is provided (change it if you want).
Usage (if it is not integrated into your editor/IDE anyway) for all files is
```
clang-format -i src/*
```

Static analysis
---------------
Static analysis can give you useful advice on improvements on your code.
For example, you can use `clang-tidy` with the provided `.clang-tidy` (which gives quite a lot of hints, you can also modify this).
First, you have to setup a compilation database using `cmake`:
```
cmake . -DCMAKE_EXPORT_COMPILE_COMMANDS=1
```

Then, usage for all files is
```
clang-tidy src/*
```

Optimum solution for instances
------------------------------
The optimum solution for the instances are\
1: 20\
2: 19\
3: 21\
4: 23\
5: 21\
6: 22\
7: 24\
8: 26\
9: 24\
10: 26\
11: 29\
12: 27
